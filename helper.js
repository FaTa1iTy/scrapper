let beatifyResponse = (status, succ, message) => {
  return { status: status, succ: succ, message: message };
};
const waitForRequests = (page, names) => {
  const requestsList = [...names];
  return new Promise(resolve =>
    page.on("request", request => {
      if (request.resourceType() === "xhr") {
        // check if request is in observed list
        const index = requestsList.indexOf(request.url());
        if (index > -1) {
          requestsList.splice(index, 1);
        }

        // if all request are fulfilled
        if (!requestsList.length) {
          resolve();
        }
      }
      request.continue();
    })
  );
};
module.exports.beatifyResponse = beatifyResponse;
module.exports.waitForRequests = waitForRequests;
