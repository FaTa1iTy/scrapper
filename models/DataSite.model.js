module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define("DataSite", {
    requrl: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false
    },
    resdata: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false
    },
    reqdata: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false
    },
    code: DataTypes.STRING
  });
  /*
  Model.associate = function(models) {
    this.api_requests = this.belongsTo(models.Statuses);
  };
  */

  return Model;
};
