module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define("Statuses", {
    code: DataTypes.STRING
  });
  Model.associate = function(models) {
    this.Statuses = this.hasMany(models.DataSite);
  };
  return Model;
};
