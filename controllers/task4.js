const express = require("express");
const router = express.Router();
const puppeteer = require("puppeteer");
const { PendingXHR } = require("pending-xhr-puppeteer");
const { DataSite } = require("../models/index");

router.post("/login", async function(req, res, next) {
  let { login, password, datebegin, dateEnd } = req.body;
  let loginF = async () => {
    // Запустим браузер

    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
      headless: true
    });

    // Откроем новую страницу
    const page = await browser.newPage();
    const pendingXHR = new PendingXHR(page);
    let pageURL = "https://publisher-dev.affluent.io";
    let result = [];
    await page.goto(pageURL);

    await page.type('input[name="username"]', login);
    await page.type('input[name="password"]', password);
    await page.click('button[type="submit"]');
    await page.waitForNavigation({
      waitUntil: "networkidle0"
    });
    await page.waitForSelector("#dashboard-report-range");
    await page.click("#dashboard-report-range");
    await page.$('input[name="daterangepicker_start"]');
    await page.click('input[name="daterangepicker_start"]', { clickCount: 3 });
    await page.type('input[name="daterangepicker_start"]', datebegin);
    await page.click('input[name="daterangepicker_end"]', { clickCount: 3 });
    await page.type('input[name="daterangepicker_end"]', dateEnd);
    await page.click("#drp_global > .range_inputs > button.applyBtn");

    await page.waitForResponse(function(response) {
      return (
        response.url() === "https://publisher-dev.affluent.io/api/query/dates"
      );
    });

    await Promise.all([
      await page.select(
        "#DataTables_Table_0_length>label>select.form-control",
        "-1"
      ),
      pendingXHR.waitForAllXhrFinished()
    ]);
    await Promise.race([
      pendingXHR.waitForAllXhrFinished(),
      new Promise(resolve => {
        setTimeout(resolve, 3500);
      })
    ]);

    //    await page.waitFor(2000);
    const rows = await scrapeMemberTable(page);

    //browser.close();

    return rows;
  };
  let finalResult;
  let requrl = "https://publisher-dev.affluent.io/api/";
  try {
    finalResult = await loginF();
    for (record of finalResult) {
      await DataSite.create({
        resdata: JSON.stringify(record),
        requrl: requrl
      });
    }
  } catch (error) {
    return next(error);
  }
  res.send(finalResult);
});
async function tableGrabber(page) {
  let result = await page.$$eval("#DataTables_Table_0>tbody>tr", trs =>
    trs.map(tr => {
      const tds = [...tr.getElementsByTagName("td")];
      return tds.map(td => td.textContent);
    })
  );
  return result;
}
async function scrapeMemberTable(page) {
  const data = await page.evaluate(async () => {
    const ths = await Array.from(
      document.querySelectorAll("#DataTables_Table_0 .heading:nth-child(1)>th")
    );
    const trs = await Array.from(
      document.querySelectorAll("#DataTables_Table_0>tbody>tr")
    );
    const headers = await ths.map(th => th.innerText);
    let results = [];
    await trs.forEach(tr => {
      let r = {};
      let tds = Array.from(tr.querySelectorAll("td")).map(td => td.innerText);
      headers.forEach((k, i) => (r[k] = tds[i]));
      results.push(r);
    });

    return results;
  });
  console.log(`Got ${data.length} records`);
  return data;
}
module.exports = router;
