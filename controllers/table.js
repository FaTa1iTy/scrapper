const express = require("express");
const router = express.Router();
const { DataSite } = require("../models/index");
router.get("/table", async function(req, res) {
  let dataTable = await DataSite.findAll({ raw: true });

  res.render("index", { table: dataTable, title: "I want work Here!" });
});

module.exports = router;
