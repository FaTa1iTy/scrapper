const express = require("express");
const router = express.Router();
const puppeteer = require("puppeteer");
const { PendingXHR } = require("pending-xhr-puppeteer");
const { DataSite } = require("../models/index");

router.get("/", async function(req, res, next) {
  let { url } = req.query;
  let scrape = async () => {
    // Запустим браузер
    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
      headless: true
    });

    // Откроем новую страницу
    const page = await browser.newPage();
    const pendingXHR = new PendingXHR(page);
    const pageURL = req.query.url;
    let obj = {};

    try {
      // Попробуем перейти по URL
      await page.goto(pageURL);
      console.log(`Открываю страницу: ${pageURL}`);
    } catch (error) {
      console.log(`Не удалось открыть
      страницу: ${pageURL} из-за ошибки: ${error}`);
      return next(error);
    }
    // Найдём все ссылки на статьи
    const postsSelector = "#console > .endpoints > ul li a";
    await page.waitForSelector(postsSelector, { timeout: 0 });
    let selector = ".output";
    let buttonName,
      result,
      arrays = [],
      counter = 0;
    const sections = await page.$$(postsSelector);
    for (const section of sections) {
      await Promise.all([section.click(), pendingXHR.waitForAllXhrFinished()]);
      await Promise.race([
        pendingXHR.waitForAllXhrFinished(),
        new Promise(resolve => {
          setTimeout(resolve, 3500);
        })
      ]);

      buttonName = await page.evaluate(section => section.innerText, section);

      result = await page.evaluate(async selector => {
        let resdata = await document.querySelector(".response > pre").innerText;
        let code = await document.querySelector(".response-code").innerText;
        let requrl = await document.querySelector(
          ".request>.request-title>strong>.link"
        ).href;
        let reqdata = await document.querySelector(".request>pre").innerText;

        return { code, resdata, requrl, reqdata };
      }, selector);
      await arrays.push(result);
    }

    browser.close();
    return arrays;
  };
  let finalResult;
  try {
    finalResult = await scrape();
    for (record of finalResult) {
      await DataSite.create(record);
    }
  } catch (error) {
    return next(error);
  }

  res.send(finalResult);
  //EVENT EMITTER
});

module.exports = router;
