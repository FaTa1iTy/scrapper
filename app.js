const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const app = express();
const db = require("./models/index");
const methods = require("./controllers/scrapper");
const methods2 = require("./controllers/task4");
const table = require("./controllers/table");
const CONFIG = require("./config/dbconn");
// view engine setup
//app.set("views", path.join(__dirname, "views"));
//app.set("view engine", "pug");

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("view engine", "pug");
app.use("/api", methods);
app.use("/api", methods2);
app.use("/api", table);
const models = require("./models/index");
models.sequelize
  .authenticate()
  .then(() => {
    console.log("Connected to SQL database:", CONFIG.db_name);
  })
  .catch(err => {
    console.error("Unable to connect to SQL database:", CONFIG.db_name, err);
  });
if (CONFIG.app === "dev") {
  //models.sequelize.sync(); //creates table if they do not already exist
  // models.sequelize.sync({ force: true }); //deletes all tables then recreates them useful for testing and development purposes
}
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  if (res.locals.result) {
    res.status(200).send(res.locals.result);
  } else {
    res.status(404).send("Проблема");
  }
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: app.get("env") === "development" ? err : {}
  });
});

module.exports = app;
